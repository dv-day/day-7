/// <reference types="Cypress"/> 
describe('My first test', function() {
    it('visit CAMT website', function() {
        cy.visit('https://camt.cmu.ac.th/th')
        cy.contains('ทุนการศึกษา')
            .click()
    })
    it('visit CAMT structure', function() {
        cy.visit('https://camt.cmu.ac.th/th')
        cy.contains('เกี่ยวกับเรา').click()
        cy.contains('โครงสร้างวิทยาลัยฯ')
            .click()
        cy.get('#Image130')
            .click()
    })
})