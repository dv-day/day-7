describe('My first test', function() {
    Cypress.on('uncaught:exception', (err, runnable) => {
        return false
    })
    it('visit CAMT structure', function() {
        cy.visit('https://itsc.cmu.ac.th/')
        cy.get('.modal-footer > .btn').click()
        cy.contains('Services')
            .click()
        cy.contains('Google Drive')
            .click()
    })
})