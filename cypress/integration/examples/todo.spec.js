/// <reference types="Cypress"/> 
describe('Test TO DO list', () => {
    it('open localhost', () => {
        cy.visit('http://localhost/')
            //Add tasks
        cy.get('[href="/task/add"]').click()
        cy.get('#summary').type('To do test 1')
        cy.get('#detail').type('test')
        cy.get('#priority').select('Normal')
        cy.get('#duedate').click()
        cy.get('#duedate').type('2020-12-12')
        cy.get('.btn').click()

        cy.get('[href="/task/add"]').click()
        cy.get('#summary').type('To do test 2')
        cy.get('#detail').type('test 2')
        cy.get('#priority').select('High')
        cy.get('#duedate').click()
        cy.get('#duedate').type('2020-12-10')
        cy.get('.btn').click()

        //Add tests
        cy.get('.container > :nth-child(2)').should('be.visible')
        cy.get('.container > :nth-child(4)').should('be.visible')
            //first card
        cy.get(':nth-child(2) > .card-body > .card-title').should('contain.text', 'To do test 2')
        cy.get(':nth-child(2) > .card-body > .card-text').should('contain.text', 'test 2')
        cy.get(':nth-child(2) > .card-header > .row > :nth-child(2) > .badge').should('contain.text', 'Priority: High')
        cy.get(':nth-child(2) > .card-header > .row > :nth-child(1)').should('contain.text', '\n            Due By:  \n            \n                2020-12-10 \n            \n        ')
            //second card
        cy.get(':nth-child(4) > .card-body > .card-title').should('contain.text', 'To do test 1')
        cy.get(':nth-child(4) > .card-body > .card-text').should('contain.text', 'test')
        cy.get(':nth-child(4) > .card-header > .row > :nth-child(2) > .badge').should('be.include.text', 'Priority: Normal')
        cy.get(':nth-child(4) > .card-header > .row > :nth-child(1)').should('contain.text', '\n            Due By:  \n            \n                2020-12-12 \n            \n        ')

        //Edit task
        cy.get(':nth-child(2) > .card-footer > .row > :nth-child(2) > .nav > :nth-child(1) > .nav-link > .fas').click()
        cy.get('#summary').clear()
        cy.get('#summary').type('Assignment submit')
        cy.get('#detail').clear()
        cy.get('#detail').type('do it')
        cy.get('#priority').select('Low')
        cy.get('#duedate').type('2020-01-20')
        cy.get('.btn').click()

        //Edit test
        cy.get(':nth-child(2) > .card-body > .card-title').should('contain.text', 'Assignment submit')
        cy.get(':nth-child(2) > .card-body > .card-text').should('contain.text', 'do it')
        cy.get(':nth-child(2) > .card-header > .row > :nth-child(2) > .badge').should('contain.text', 'Priority: Low')
        cy.get(':nth-child(2) > .card-header > .row > :nth-child(1)').should('contain.text', '\n            Due By:  \n            \n                2020-01-20 \n            \n        ')

        //Delete task
        cy.get(':nth-child(4) > .card-footer > .row > :nth-child(2) > .nav > [cl=""] > .nav-link > .fas').click()
        cy.get('.btn-danger').click()

        //Delete test
        cy.get(':nth-child(4) > .card-body').should('not.exist')

        //Mark as complete
        cy.get('.btn').click()

        cy.get('h4 > .badge').should('contain.text', 'Complete')
        cy.get(':nth-child(1) > .nav-link > .fa-edit').should('not.exist')
    })

})