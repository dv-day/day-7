describe('My first test', function() {
    it('visit CAMT structure', function() {
        cy.visit('https://currency-exchange-969bb.web.app/currency')
            //THB to THB
        cy.get('#sourceAmount').type('100')
        cy.get('#calBtn').click()
        cy.get('#sourceAmount').clear()

        cy.get('#result').should('be.visible')
            .and('contain.text', '\n      100.00 THB\n    ')

        //THB to USD
        cy.get('#sourceAmount').type('200')
        cy.get('#selectTarget').click()
        cy.get('#targetUSD').click()
        cy.get('#calBtn').click()
        cy.get('#sourceAmount').clear()

        cy.get('#result').should('be.visible')
            .and('contain.text', '\n      6.6667 USD\n    ')

        //THB to  EUR
        cy.get('#sourceAmount').type('300')
        cy.get('#selectTarget').click()
        cy.get('#targetEUR').click()
        cy.get('#calBtn').click()
        cy.get('#sourceAmount').clear()

        cy.get('#result').should('be.visible')
            .and('contain.text', '\n      9.00 EUR\n    ')

        //USD to THB
        cy.get('#sourceAmount').type('400')
        cy.get('#selectSource').click()
        cy.get('#sourceUSD').click()
        cy.get('#selectTarget').click()
        cy.get('#targetTHB').click()
        cy.get('#calBtn').click()
        cy.get('#sourceAmount').clear()

        cy.get('#result').should('be.visible')
            .and('contain.text', '\n      12,000.00 THB\n    ')

        //USD to USD
        cy.get('#sourceAmount').type('500')
        cy.get('#selectTarget').click()
        cy.get('#targetUSD').click()
        cy.get('#calBtn').click()
        cy.get('#sourceAmount').clear()

        cy.get('#result').should('be.visible')
            .and('contain.text', '\n      500.00 USD\n    ')

        //USD to EUR
        cy.get('#sourceAmount').type('600')
        cy.get('#selectTarget').click()
        cy.get('#targetEUR').click()
        cy.get('#calBtn').click()
        cy.get('#sourceAmount').clear()

        cy.get('#result').should('be.visible')
            .and('contain.text', '\n      540.00 EUR\n    ')

        //EUR to THB
        cy.get('#sourceAmount').type('700')
        cy.get('#selectSource').click()
        cy.get('#sourceEUR').click()
        cy.get('#selectTarget').click()
        cy.get('#targetTHB').click()
        cy.get('#calBtn').click()
        cy.get('#sourceAmount').clear()

        cy.get('#result').should('be.visible')
            .and('contain.text', '\n      23,333.3333 THB\n    ')

        //EUR to USD

        cy.get('#sourceAmount').type('800')
        cy.get('#selectTarget').click()
        cy.get('#targetUSD').click()
        cy.get('#calBtn').click()
        cy.get('#sourceAmount').clear()

        cy.get('#result').should('be.visible')
            .and('contain.text', '\n      888.8889 USD\n    ')

        //EUR to EUR

        cy.get('#sourceAmount').type('900')
        cy.get('#selectTarget').click()
        cy.get('#targetEUR').click()
        cy.get('#calBtn').click()
        cy.get('#sourceAmount').clear()

        cy.get('#result').should('be.visible')
            .and('contain.text', '\n      900.00 EUR\n    ')
    })
})